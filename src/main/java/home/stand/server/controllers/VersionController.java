package home.stand.server.controllers;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class VersionController {

    @GetMapping("/version")
    @PreAuthorize("hasAuthority('permission:read') || hasRole('ADMIN')")
    // @PreAuthorize("hasRole('ADMIN')")
    public String getMethodName() {
        return "0.0.1";
    }

}
